Po stažení tohoto repositáře otevřete složku VR Splat za pomoci Unity Hub nebo přímo Unity.
Tato složka obsahuje celý projekt.

Pro otevření doporučuji použít verzi Unity, které tento projekt doporučuje, neboť v ní byl vytvořen (jedná se o verzi 2020.3.21f1).

Po otevření tohoto projektu otevřete v prohlížeči v dolní části Unity rozhraní složku Assets>Scenes a Poklikejte na scénu jménem "Paint TNTC"
Toto je testovací scéna. Po kliknutí na ikonu trojúhelníku v horní části Unity rozhraní by se mělo zapnout SteamVR a do VR brýlí by se měla začít promítat scéna. Pokud nedojde k zapnutí SteamVR, je třeba jít do záložky Edit>Project Settings>XR Plug-In Management, odkliknout OpenVR Loader, zapnout scénu, vypnout jí a opět tuto možnost zaškrtnout. Poté by se již projekt měl zapnout v pořádku.

V tomto projektu se nastavují parametry štětce v objektu  "MousePainter" za pomocí skriptu "Mouse Painter", kde lze opět nastavit barvu, tvrdost, či průhlednost barvy.
Pro správvné chování doporučuji držet Sílu i Tvrdost štětce v rozmezí od 0 do 1

- Ovládání (za pomoci pravého ovladače):
- Přední trigger - volné kreslení
- Boční trigger - kreslení s vodítky
- Trackpad (využívaný jako D-Pad):
- Horní - Zvětšní plátna
- Dolní - Zmenšení plátna 
- Klávesnice:
- r - +červená
- t - +zelená
- z - +modrá
- f - -červená
- g - -zelená
- h - -modrá

