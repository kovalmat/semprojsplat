using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
//object scale script created by Matyas Koval 2022
public class Scale : MonoBehaviour
{
    public Vector3 ScaleFactor = new Vector3(0.01f, 0, 0.01f);
    private bool up;
    private bool down;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (SteamVR_Actions._default.ScaleUp[SteamVR_Input_Sources.RightHand].stateDown)
        {
            up = true;

        }
        else if(SteamVR_Actions._default.ScaleUp[SteamVR_Input_Sources.RightHand].stateUp)
        {
            up = false;
        }
        if (up)
        {
            gameObject.transform.localScale += ScaleFactor;
        }
        
        if (SteamVR_Actions._default.ScaleDown[SteamVR_Input_Sources.RightHand].stateDown)
        {
            down = true;

        }
        else if(SteamVR_Actions._default.ScaleDown[SteamVR_Input_Sources.RightHand].stateUp)
        {
            down = false;
        }
        if (down)
        {
            gameObject.transform.localScale -= ScaleFactor;
        }
    }
}
