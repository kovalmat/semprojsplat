using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//created by Matyas Koval 2021
// a script for the visualisation of the raycast from camera in a controller, with which the user is painting
public class PhysicsPointer : MonoBehaviour
{
    public float defaultLength = 100.0f;
    public MousePainter painter;

    private LineRenderer lineRenderer = null;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        UpdateLength();
    }

    private void UpdateLength()
    {
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, calculateEnd());
    }

    private Vector3 calculateEnd()
    {
        RaycastHit hit = CreateForwardRaycast();
        Vector3 endPosition = DefaultEnd(defaultLength);

        if (hit.collider)
        {
            endPosition = hit.point;
            
            /* painter.p = hit.collider.GetComponent<Paintable>();*/
        }

        painter.hitPoint = endPosition;
        return endPosition;
    }

    private RaycastHit CreateForwardRaycast()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        Physics.Raycast(ray, out hit, defaultLength);
        return hit;
    }

    private Vector3 DefaultEnd(float length)
    {
        return transform.position + (transform.forward * defaultLength);
    }
}
