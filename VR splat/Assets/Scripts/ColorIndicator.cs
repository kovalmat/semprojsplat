using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Created by Matzas Koval 2022
//A simple script for the visualisation of the selected pen colour in a material
public class ColorIndicator : MonoBehaviour
{
    public Material mat;
    private MousePainter painter;
    // Start is called before the first frame update
    void Start()
    {
        painter = GameObject.FindObjectOfType<MousePainter>();
    }

    // Update is called once per frame
    void Update()
    {
        mat.color = painter.paintColor;
        
    }
}
