using System.Collections;
using UnityEngine;
using Valve.VR;

//script found in the original project, heavily modified bz Matyas Koval 2021
//script handling painting, originally with a mouse
public class MousePainter : MonoBehaviour
{
    public Camera cam;
    public Camera VRcam;
    [Space]
    public bool mouseSingleClick;
    public SteamVR_Action_Boolean action;
    [Space]
    public Color paintColor;

    public float radius = 1;
    public float strength = 1;
    public float hardness = 1;

    public Vector3 hitPoint;

    private Vector3 positionNew;
    private Vector3 positionOld;

    private Vector3 startLine;
    private Vector3 endLine;
    private bool up;
    private bool down;

    private void Start()
    {
        gameObject.GetComponent<LineRenderer>().enabled = false; //linerenderer for visualisation of drawing with control points
    }

    void Update()
    {

        bool click;
        //click = mouseSingleClick ? Input.GetMouseButtonDown(0) : Input.GetMouseButton(0);
        click = SteamVR_Actions._default.GrabPinch[SteamVR_Input_Sources.RightHand].state;



        //coroutine for free drawing
        if (click)
        {
            positionNew = /*Input.mousePosition*/hitPoint;
            if(positionOld != Vector3.zero)
            {
                StartCoroutine(LerpPosition(positionNew, 0.3f));
            }
            
            positionOld = positionNew;
        } else // if user is not free drawing, check if he is drawing using control points
        {
            positionOld = positionNew = new Vector3(0,0,0);

            if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateDown && startLine == Vector3.zero)
            {
                if (gameObject.GetComponent<LineRenderer>().enabled != true)
                {
                    gameObject.GetComponent<LineRenderer>().enabled = true;
                }
                startLine = hitPoint;
                //cam.transform.position = endPos;
                gameObject.GetComponent<LineRenderer>().SetPosition(0, startLine);
                
                down = true;

            }
            else if (SteamVR_Actions._default.GrabGrip[SteamVR_Input_Sources.RightHand].stateUp && endLine == Vector3.zero)
            {
                endLine = hitPoint;
                up = true;
            }

            //during the drawing with control points, show the line renderer
            if (down)
            {
                gameObject.GetComponent<LineRenderer>().SetPosition(1, hitPoint);
            }
            //coroutine for drawing straight lines
            if (endLine != Vector3.zero && startLine != Vector3.zero && down && up)
            {
                down = false;
                up = false;
                Debug.Log("start " + startLine);
                Debug.Log("end " + endLine);
                StartCoroutine(LerpPosition(startLine, endLine, Vector3.Distance(startLine, endLine)*0.2f));
            }
        }
        //user modification of colour and alpha channel during runtime
        if (Input.GetKey("r"))
        {
            Debug.Log("red");
            paintColor.r += 0.001f;
        }
        if (Input.GetKey("t"))
        {
            paintColor.g += 0.001f;
        }
        if (Input.GetKey("y"))
        {
            paintColor.b += 0.001f;
        }
        if (Input.GetKey("f"))
        {
            paintColor.r -= 0.001f;
        }
        if (Input.GetKey("g"))
        {
            paintColor.g -= 0.001f;
        }
        if (Input.GetKey("h"))
        {
            paintColor.b -= 0.001f;
        }
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            paintColor.a -= 0.001f;
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            paintColor.a += 0.001f;
        }
    }

    //coroutine for freedrawing
    IEnumerator LerpPosition(Vector2 targetPosition, float duration)
    {
        float time = 0;
        Vector2 startPosition = positionOld;

        while (time < duration)
        {
            transform.position = Vector2.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //Debug.DrawRay(ray.origin, hit.point - ray.origin, Color.red);
                //transform.position = hit.point;
                Paintable p = hit.collider.GetComponent<Paintable>();
                if (p != null)
                {
                    PaintManager.instance.paint(p, hit.point, radius, hardness, strength, paintColor);
                }
            }
            yield return null;
        }
    }

    //coroutine for draiwng straight lines
    IEnumerator LerpPosition(Vector2 startPosition, Vector2 targetPosition, float duration)
    {
        gameObject.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, Vector3.zero);
        float time = 0;
        while (time < duration)
        {
            transform.position = Vector2.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //Debug.DrawRay(ray.origin, hit.point - ray.origin, Color.red);
                //transform.position = hit.point;
                Paintable p = hit.collider.GetComponent<Paintable>();
                if (p != null)
                {
                    PaintManager.instance.paint(p, hit.point, radius, hardness, strength, paintColor);
                }
            }
            yield return null;
            startLine = endLine = Vector3.zero;
            
        }
    }

}
